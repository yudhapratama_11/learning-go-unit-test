package helper

import (
	"fmt"
	"testing"
)

func TestHelloWorld(t *testing.T) {
	result := HelloWorld("Yosep")
	if result != "Hello Yudha" {
		t.Error("Harus Hello Yudha") // Menandakan kalau error
	}

	fmt.Println("TestHelloWorld Done")
}
