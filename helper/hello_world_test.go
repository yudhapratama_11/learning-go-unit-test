package helper

import (
	"testing"
)

// func BenchmarkHelloWorld(b *testing.B) {
// 	for i := 0; i < b.N; i++ {
// 		HelloWorld("Yudha")
// 	}
// }

// func BenchmarkHelloPratama(b *testing.B) {
// 	for i := 0; i < b.N; i++ {
// 		HelloWorld("Pratama")
// 	}
// }

// func BenchmarkSub(b *testing.B) {
// 	b.Run("Yudha", func(b *testing.B) {
// 		for i := 0; i < b.N; i++ {
// 			HelloWorld("Yudha")
// 		}
// 	})
// 	b.Run("Pratama", func(b *testing.B) {
// 		for i := 0; i < b.N; i++ {
// 			HelloWorld("Pratama")
// 		}
// 	})
// }

// func TestMain(m *testing.M) { // Menambahkan before after pada test
// 	// before
// 	fmt.Println("Before Unit Test")

// 	m.Run()
// 	//fmt.Println(data)

// 	// after
// 	fmt.Println("After Unit Test")
// }

// func TestHelloWorld(t *testing.T) {
// 	result := HelloWorld("Yudha")
// 	if result != "Hello Yudha1" {
// 		//error
// 		// t.Fail()
// 		t.Error("Harus Hello Yudha") // setelah error dia panggil t.fail()
// 	}
// 	fmt.Println("TestHelloWorld Done")
// }

// func TestHelloWorldYudha(t *testing.T) {
// 	result := HelloWorld("Pratama")
// 	if result != "Hello Pratama1" {
// 		//error
// 		// t.FailNow()
// 		t.Fatal("Harunsya Hello Pratama") // setelah error dia panggil t.failNow()
// 	}
// 	fmt.Println("TestHelloWorldYudha Done")
// }

// func TestHelloWorldAssertion(t *testing.T) {
// 	result := HelloWorld("Pratama1")
// 	assert.Equal(t, "Hello Pratama", result, "Result must be 'Hello Pratama'") // kalaun error dia panggil t.fail()
// 	fmt.Println("TestHelloWorldAssertion Done")
// }

// func TestHelloWorldRequire(t *testing.T) {
// 	result := HelloWorld("Yudha1")
// 	require.Equal(t, "Hello Yudha", result, "Result must be 'Hello Yudha'") // kalaun error dia panggil t.FailNow()
// 	fmt.Println("TestHelloWorldRequire Done")
// }

// func TestSkip(t *testing.T) {
// 	fmt.Println(runtime.GOOS)
// 	if runtime.GOOS == "linux" {
// 		t.Skip("Unit test tidak bisa jalan di Windows")
// 		// fmt.Println("Masuk Fungsi")
// 	}
// 	result := HelloWorld("Yudha")                                           // kalau gak di t.skip, ini akan jalan
// 	require.Equal(t, "Hello Yudha", result, "Result must be 'Hello Yudha'") // kalaun error dia panggil t.FailNow()
// 	fmt.Println("TestHelloWorldRequire Done")
// 	fmt.Println("Masuk")
// }

// func TestSubTest(t *testing.T) {
// 	t.Run("Yudha", func(t *testing.T) {
// 		result := HelloWorld("Yudha")
// 		require.Equal(t, "Hello Yudha", result, "Result must be 'Hello Yudha'")
// 	})
// 	t.Run("Pratama", func(t *testing.T) {
// 		result := HelloWorld("Pratama1")
// 		require.Equal(t, "Hello Pratama", result, "Result must be 'Hello Pratama'")
// 	})
// }

// func TestHelloWorldTable(t *testing.T) {
// 	tests := []struct {
// 		name     string
// 		request  string
// 		expected string
// 	}{
// 		{
// 			name:     "HelloWorld(Yudha)",
// 			request:  "Yudha",
// 			expected: "Hello Yudha",
// 		},
// 		{
// 			name:     "Pratama",
// 			request:  "Pratama1",
// 			expected: "Hello Pratama",
// 		},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			result := HelloWorld(test.request)
// 			assert.Equal(t, test.expected, result, "Result must be "+test.expected)
// 		})
// 	}
// }

func BenchmarkHelloWorldTable(t *testing.B) {
	tests := []struct {
		name    string
		request string
	}{
		{
			name:    "Yudha",
			request: "Yudha",
		},
		{
			name:    "Pratama",
			request: "Pratama",
		},
		{
			name:    "Coba",
			request: "Coba",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.B) {
			for i := 0; i < t.N; i++ {
				HelloWorld(test.request)
			}
		})
	}
}
