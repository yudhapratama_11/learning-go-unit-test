package helper

import (
	"runtime"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSkip(t *testing.T) {
	if runtime.GOOS != "ubuntu" {
		t.Skip("Unit test tidak bisa jalan di Windows")
	}

	result := HelloWorld("Yudha")
	require.Equal(t, "Hello Yudha", result)
}
