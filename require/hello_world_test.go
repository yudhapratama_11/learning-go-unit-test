package helper

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestHelloWorldRequire(t *testing.T) {
	result := HelloWorld("Yudha")
	require.Equal(t, "Hello Yudha", result)

	fmt.Println("Tidak Dieksekusi")
}
