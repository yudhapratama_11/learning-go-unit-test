package helper

import (
	"fmt"
	"testing"
)

func TestHelloWorld(t *testing.T) {
	result := HelloWorld("Yudha")
	if result != "Hello Yudha" {
		panic("Harus Hello Yudha") // Menandakan kalau error
	}

	fmt.Println("TestHelloWorld Done")
}

func TestHelloWorld2(t *testing.T) {
	result := HelloWorld("Yudha")
	if result != "Hello Yudha" {
		panic("Harus Hello Yudha") // Menandakan kalau error
	}

	fmt.Println("TestHelloWorld Done")
}
