package service

import (
	"belajar-golang-unit-test/mock/entity"
	"belajar-golang-unit-test/mock/repository"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var categoryRepository = &repository.CategoryRepositoryMock{Mock: mock.Mock{}}
var categoryService = CategoryService{Repository: categoryRepository}

func TestCategoryService_GetNotFound(t *testing.T) {
	// Semacam simpan data, kalo misal findbyid 1 langsung diset sbg nil
	categoryRepository.Mock.On("FindById", "1").Return(nil)
	// categoryRepository.Mock.On("FindById", "3").Return(nil)
	category, err := categoryService.Get("1")

	assert.Nil(t, category) // if category == nil return true, if not return fail()
	assert.NotNil(t, err)   // if err != nil return true, if not return fail()
}

func TestCategoryService_GetFound(t *testing.T) {
	// Expected Response
	category := entity.Category{
		Id:   "2",
		Name: "Laptop",
	}
	categoryRepository.Mock.On("FindById", "2").Return(category) // Semacam simpan data, kalo misal findbyid 2 langsung diset data dari param category
	//categoryRepository.Mock.On("FindById", "3").Return(nil)
	result, err := categoryService.Get("2")

	assert.Nil(t, err)       // if err == nil return true, if not return fail()
	assert.NotNil(t, result) // if category != nil return true, if not return fail()
	assert.Equal(t, category.Id, result.Id)
	assert.Equal(t, category.Name, result.Name)

	// result1, err1 := categoryService.Get("3")

	// assert.Nil(t, err1)
	// assert.NotNil(t, result1) // if category != nil return true, if not return fail()
	// assert.Equal(t, category.Id, result1.Id)
	// assert.Equal(t, category.Name, result.Name)
}
