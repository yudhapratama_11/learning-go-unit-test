package repository

import "belajar-golang-unit-test/mock/entity"

type CategoryRepository interface {
	FindById(id string) *entity.Category
}
