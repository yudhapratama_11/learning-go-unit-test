package repository

import (
	"belajar-golang-unit-test/mock/entity"

	"github.com/stretchr/testify/mock"
)

type CategoryRepositoryMock struct {
	Mock mock.Mock
}

// Fungsi ini buat cek data dengan id dari input yang akan dimasukan di service layer
func (repository *CategoryRepositoryMock) FindById(id string) *entity.Category {
	arguments := repository.Mock.Called(id) // call all mock
	if arguments.Get(0) == nil {
		return nil
	} else {
		category := arguments.Get(0).(entity.Category)
		return &category
		//nilai category diambil address pointernya untuk direturnkan ke nilai yang berbentu pointer
	}
}
